# klettern-von-steyr-bis-weyer

The book project and a maintenance platform for all climbing topos at klettern-im-ennstal.at.

See the official book website at [klettern-von-steyr-bis-weyer.at](http://klettern-von-steyr-bis-weyer.at/)

### Contributing
Contribute with a pull request. 


### Communication: Matrix Chat
This project communicates through a matrix.org chat room, gitlab and email.

Link: [Matrix Chat](https://matrix.to/#/#crystywey:matrix.org)
(Login or click at Continue, Continue, Continue in your Browser)


## Roadmap
- Continue updating topos, release every new version on the website klettern-im-ennstal.at
- Add new regions

## Installation
Use Inkscape for the topos, use TeXstudio or a text editor for the texts.

LaTeX editing in TeXstudio:
![TeXstudio](Screenshot_example_TeXstudio.png)

Code versioning in VSCode:
![VS Code](Screenshot_example_vscode.png)

Topo editing in Inkscape:
![Inkscape](Screenshot_example_inkscape_topo_template.png)
Editing the template file for topos.

## Authors and acknowledgment
Contributors will be mentioned in the foreword of the next release. 

## License
CC-BY-NC-SA-4.0 

## Project status
The first version of the book is released and can be aquired at various shops. See the list on the official book website at [klettern-von-steyr-bis-weyer.at](http://klettern-von-steyr-bis-weyer.at/)


